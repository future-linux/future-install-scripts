# Future Linux Install Scripts

根据 arch-install-scripts 修改而来

## Requirements

* GNU coreutils (>= v8.15)
* util-linux (>= 2.39)
* POSIX awk
* bash (>= 4.1)
* python-asciidoc (for generating man pages)

## License

See COPYING for details.
